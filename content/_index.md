---
title: "Diplomacy Score Calculators"
description: "Calculators for various Diplomacy scoring systems"
---

{{< rawhtml >}}

<!-- Shoutout to https://windycityweasels.org/tribute-scoring-system/ 
		for inspiring me to make this website -->
<div id="game-url-form">
	<form name="gameUrlForm" onsubmit="return false">
		<fieldset>
			<legend>Paste your game URL here:</legend>
			<label>
				<input id="gameUrlInput" type="URL" placeholder="Supports WebDip, vDip, and Backstabbr URLs" onclick="this.select()" autofocus>
			</label>
			<button id="submitButton" onclick="scrapeGameScores()">Submit</button>
		</fieldset>
	</form>
</div>
<div>
	<p>Click to add/remove scoring systems</p>
	<!-- <button button="toggle-button" onclick="toggleScoringSystem('armada')">Armada</button> -->
	<button class="scoring-system-button" button="toggle-button" onclick="toggleScoringSystem('ppsc')">PPSC</button>
	<button class="scoring-system-button" button="toggle-button" onclick="toggleScoringSystem('sos')">SoS</button>
	<button class="scoring-system-button" button="toggle-button" onclick="toggleScoringSystem('mind-the-gap')">Mind the Gap</button>
	<button class="scoring-system-button" button="toggle-button" onclick="toggleScoringSystem('open-tribute')">OpenTribute</button>
	<button class="scoring-system-button" button="toggle-button" onclick="toggleScoringSystem('omg')">OMG</button>
</div>
<hr id="hr1">
<div id="scoring-systems">
	<table id="scoring-table">
		<tbody>
			<tr class="scoring-row">
				<td id="empty-td" class="scoring-column"></td>
				<!-- some scoring systems are hidden initally via CSS -->
				<td class="scoring-column">⬇️ || ⬆️</td>
				<td class="scoring-column">Centers</td>
				<td class="scoring-column column-break"></td>
				<td class="scoring-column year-eliminated">Elim in:</td>
				<td class="scoring-column column-break"></td>
				<td class="scoring-column armada">Armada</td>
				<td class="scoring-column ppsc">PPSC</td>
				<td class="scoring-column sos">SoS (pot: 70)</td>
				<td class="scoring-column mind-the-gap">Mind the Gap</td>
				<td class="scoring-column open-tribute">OpenTribute</td>
				<td class="scoring-column omg">OMG*</td>
			</tr>
			<tr class="scoring-row austria-color">
				<td class="scoring-column">Austria</td>
				<td class="scoring-column">
					<button class="center-count-button" onclick="updateCenterCounts('austria',-1)">⬇️</button> || 
					<button class="center-count-button" onclick="updateCenterCounts('austria',1)">⬆️</button>
				</td>
				<td id="austria-center-count" class="scoring-column">3</td>
				<td class="scoring-column column-break"></td>
				<td class="scoring-column year-eliminated">
					<input id="austria-year-elim" class="year-eliminated-input" type="number" value="1904" min="1901">
				</td>
				<td class="scoring-column column-break"></td>
				<td id="austria-score-armada" class="scoring-column armada">?</td>
				<td id="austria-score-ppsc" class="scoring-column ppsc">3</td>
				<td id="austria-score-sos" class="scoring-column sos">10</td>
				<td id="austria-score-mind-the-gap" class="scoring-column mind-the-gap">10.50</td>
				<td id="austria-score-open-tribute" class="scoring-column open-tribute">42</td>
				<td id="austria-score-omg" class="scoring-column omg">42</td>
			</tr>
			<tr class="scoring-row england-color">
				<td class="scoring-column">England</td>
				<td class="scoring-column">
					<button class="center-count-button" onclick="updateCenterCounts('england',-1)">⬇️</button> || 
					<button class="center-count-button" onclick="updateCenterCounts('england',1)">⬆️</button>
				</td>
				<td id="england-center-count" class="scoring-column">3</td>
				<td class="scoring-column column-break"></td>
				<td class="scoring-column year-eliminated">
					<input id="england-year-elim" class="year-eliminated-input" type="number" value="1904" min="1901">
				</td>
				<td class="scoring-column column-break"></td>
				<td id="england-score-armada" class="scoring-column armada">?</td>
				<td id="england-score-ppsc" class="scoring-column ppsc">3</td>
				<td id="england-score-sos" class="scoring-column sos">10</td>
				<td id="england-score-mind-the-gap" class="scoring-column mind-the-gap">10.50</td>
				<td id="england-score-open-tribute" class="scoring-column open-tribute">42</td>
				<td id="england-score-omg" class="scoring-column omg">42</td>
			</tr>
			<tr class="scoring-row france-color">
				<td class="scoring-column">France</td>
				<td class="scoring-column">
					<button class="center-count-button" onclick="updateCenterCounts('france',-1)">⬇️</button> || 
					<button class="center-count-button" onclick="updateCenterCounts('france',1)">⬆️</button>
				</td>
				<td id="france-center-count" class="scoring-column">3</td>
				<td class="scoring-column column-break"></td>
				<td class="scoring-column year-eliminated">
					<input id="france-year-elim" class="year-eliminated-input" type="number" value="1904" min="1901">
				</td>
				<td class="scoring-column column-break"></td>
				<td id="france-score-armada" class="scoring-column armada">?</td>
				<td id="france-score-ppsc" class="scoring-column ppsc">3</td>
				<td id="france-score-sos" class="scoring-column sos">10</td>
				<td id="france-score-mind-the-gap" class="scoring-column mind-the-gap">10.50</td>
				<td id="france-score-open-tribute" class="scoring-column open-tribute">42</td>
				<td id="france-score-omg" class="scoring-column omg">42</td>
			</tr>
			<tr class="scoring-row germany-color">
				<td class="scoring-column">Germany</td>
				<td class="scoring-column">
					<button class="center-count-button" onclick="updateCenterCounts('germany',-1)">⬇️</button> || 
					<button class="center-count-button" onclick="updateCenterCounts('germany',1)">⬆️</button>
				</td>
				<td id="germany-center-count" class="scoring-column">3</td>
				<td class="scoring-column column-break"></td>
				<td class="scoring-column year-eliminated">
					<input id="germany-year-elim" class="year-eliminated-input" type="number" value="1904" min="1901">
				</td>
				<td class="scoring-column column-break"></td>
				<td id="germany-score-armada" class="scoring-column armada">?</td>
				<td id="germany-score-ppsc" class="scoring-column ppsc">3</td>
				<td id="germany-score-sos" class="scoring-column sos">10</td>
				<td id="germany-score-mind-the-gap" class="scoring-column mind-the-gap">10.50</td>
				<td id="germany-score-open-tribute" class="scoring-column open-tribute">42</td>
				<td id="germany-score-omg" class="scoring-column omg">42</td>
			</tr>
			<tr class="scoring-row italy-color">
				<td class="scoring-column">Italy</td>
				<td class="scoring-column">
					<button class="center-count-button" onclick="updateCenterCounts('italy',-1)">⬇️</button> || 
					<button class="center-count-button" onclick="updateCenterCounts('italy',1)">⬆️</button>
				</td>
				<td id="italy-center-count" class="scoring-column">3</td>
				<td class="scoring-column column-break"></td>
				<td class="scoring-column year-eliminated">
					<input id="italy-year-elim" class="year-eliminated-input" type="number" value="1904" min="1901">
				</td>
				<td class="scoring-column column-break"></td>
				<td id="italy-score-armada" class="scoring-column armada">?</td>
				<td id="italy-score-ppsc" class="scoring-column ppsc">3</td>
				<td id="italy-score-sos" class="scoring-column sos">10</td>
				<td id="italy-score-mind-the-gap" class="scoring-column mind-the-gap">10.50</td>
				<td id="italy-score-open-tribute" class="scoring-column open-tribute">42</td>
				<td id="italy-score-omg" class="scoring-column omg">42</td>
			</tr>
			<tr class="scoring-row russia-color">
				<td class="scoring-column">Russia</td>
				<td class="scoring-column">
					<button class="center-count-button" onclick="updateCenterCounts('russia',-1)">⬇️</button> || 
					<button class="center-count-button" onclick="updateCenterCounts('russia',1)">⬆️</button>
				</td>
				<td id="russia-center-count" class="scoring-column">4</td>
				<td class="scoring-column column-break"></td>
				<td class="scoring-column year-eliminated">
					<input id="russia-year-elim" class="year-eliminated-input" type="number" value="1904" min="1901">
				</td>
				<td class="scoring-column column-break"></td>
				<td id="russia-score-armada" class="scoring-column armada">?</td>
				<td id="russia-score-ppsc" class="scoring-column ppsc">4</td>
				<td id="russia-score-sos" class="scoring-column sos">10</td>
				<td id="russia-score-mind-the-gap" class="scoring-column mind-the-gap">19.50</td>
				<td id="russia-score-open-tribute" class="scoring-column open-tribute">52</td>
				<td id="russia-score-omg" class="scoring-column omg">52</td>
			</tr>
			<tr class="scoring-row turkey-color">
				<td class="scoring-column">Turkey</td>
				<td class="scoring-column">
					<button class="center-count-button" onclick="updateCenterCounts('turkey',-1)">⬇️</button> || 
					<button class="center-count-button" onclick="updateCenterCounts('turkey',1)">⬆️</button>
				</td>
				<td id="turkey-center-count" class="scoring-column">3</td>
				<td class="scoring-column column-break"></td>
				<td class="scoring-column year-eliminated">
					<input id="turkey-year-elim" class="year-eliminated-input" type="number" value="1904" min="1901">
				</td>
				<td class="scoring-column column-break"></td>
				<td id="turkey-score-armada" class="scoring-column armada">?</td>
				<td id="turkey-score-ppsc" class="scoring-column ppsc">3</td>
				<td id="turkey-score-sos" class="scoring-column sos">10</td>
				<td id="turkey-score-mind-the-gap" class="scoring-column mind-the-gap">10.50</td>
				<td id="turkey-score-open-tribute" class="scoring-column open-tribute">42</td>
				<td id="turkey-score-omg" class="scoring-column omg">42</td>
			</tr>
			<tr class="scoring-row">
				<td class="scoring-column">Total</td>
				<td id="desktop-arrows" class="scoring-column">----------</td>
				<td id="total-center-count" class="scoring-column">22</td>
				<td class="scoring-column column-break"></td>
				<td class="scoring-column year-eliminated">----</td>
				<td class="scoring-column column-break"></td>
				<td id="total-score-armada" class="scoring-column armada">????</td>
				<td id="total-score-ppsc" class="scoring-column ppsc">22</td>
				<td id="total-score-sos" class="scoring-column sos">70</td>
				<td id="total-score-mind-the-gap" class="scoring-column mind-the-gap">82.50</td>
				<td id="total-score-open-tribute" class="scoring-column open-tribute">304</td>
				<td id="total-score-omg" class="scoring-column omg">304</td>
			</tr>
		</tbody>
	</table>
	<hr id="hr2">
	<p><u>Links to some scoring systems:</u></p>
	<!-- <a href="http://armada-dip.com/scoring.html">Armada</a> |  -->
	<a href="https://webdiplomacy.net/points.php#SoS">Sum of Squares (SoS)</a> | 
	<a href="https://diplobn.com/opentribute-scoring-system/">OpenTribute</a>
	<hr id="hr3">
	<p>* OMG stands for Open Mind the Gap, a scoring system first tested during the 2021 July-August Nexus Speedboat season. The points awarded for a solo <i>may differ</i> between Nexus events, so please check the rules for your competition.</p>
</div>

<script type="text/javascript">
	// Yeah all the javascript is here instead of in a separate file...sorry

	/* Effectively global variables */

	// Country order matters, it's assumed by the other functions
	let countryNames = ["austria", "england", "france", "germany", "italy", "russia", "turkey"];
	let centerCountsNumbers = {};
	let centerCountsElements = {};
	let totalCenters = 0;
	let pot = 70;	// Used for SoS (and maybe more in the future?)
	let yearElimSystemsCount = 0; // Track the # of visible systems using year elim so we don't accidently hide it

	// Scrape the scores from a game and set the scoring table to those scores
	async function scrapeGameScores() {
		url = document.getElementById("gameUrlInput").value;
		scrapedCenterCounts = {"austria": 0, "england": 0, "france": 0, "germany": 0,
			"italy": 0, "russia": 0, "turkey": 0}

		// Update button text to "Processing"
		document.getElementById("submitButton").innerHTML = "Processing..."

		try {
			// This cors-anywhere server is run via Heroku app and can be found here:
			// https://gitlab.com/diplomacy-things/cors-anywhere-fork
			let response = await fetch("https://cors-anywhere-diplomacy-things.herokuapp.com/" + url);
    		let data = await response.text();

    		var webpage = document.createElement("html");
    		webpage.innerHTML = data;

    		// Let's parse :tada:
    		if (url.includes("webdiplomacy.")) {
    			parseWebDipGame(webpage, scrapedCenterCounts);
			} else if (url.includes("vdiplomacy.")) {
				parseVDipGame(webpage, scrapedCenterCounts);
    		} else if (url.includes("backstabbr.")) {
    			parseBackstabbrGame(webpage, scrapedCenterCounts);
    		}

		} catch(err) {
			console.log(err);
		}

		setCenterCounts(scrapedCenterCounts);

		// New center counts? Need to update the scores!
		updateScores();

		// Update button text back to "Submit"
		document.getElementById("submitButton").innerHTML = "Submit";

		// Turned off clearing the URL because maybe people would want to refresh the same game?
		// document.getElementById("gameUrlInput").value = null;
	}

    // you have to find ongoing games for complete testing e.g. retreats
	// test URLs
	// webdip, drawn game: https://webdiplomacy.net/board.php?gameID=340030
	// webdip, won game: https://webdiplomacy.net/board.php?gameID=334382
	// webdip, game with a CD: https://webdiplomacy.net/board.php?gameID=404806
	function parseWebDipGame(webpage, scrapedCenterCounts) {
		let table = webpage.getElementsByClassName("membersFullTable")[0].children[0];
		// console.log(table);
		let rows = table.getElementsByClassName("member");

		// Go through each country to get the name and center count
		for (var i = rows.length - 1; i >= 0; i--) {
			let span = rows[i].getElementsByClassName("memberCountryName")[0];
			console.log(rows[i]);
			let countryName = span.textContent.trim().toLowerCase();

			// Handles a weird dash during retreat phases
			if (countryName[0] == "-") {
				countryName = countryName.split(" ")[1];
			}

			let scCount = "0";
			try {
				let text = rows[i].getElementsByClassName("memberSCCount")[0].innerHTML;
				scCount = text.split("</em>")[0].substr(4);
			} catch(err) {
				// Error? Assume no field and 0 centers
				scCount = "0";
			}
			scrapedCenterCounts[countryName] = Number(scCount);
		}
	}

	// test URLs
	// vdip, drawn game: https://vdiplomacy.com/board.php?gameID=50673
	// vdip, won game: https://vdiplomacy.com/board.php?gameID=50129
	function parseVDipGame(webpage, scrapedCenterCounts) {
    	let table = webpage.getElementsByClassName("membersFullTable")[0].children[0];
		let rows = table.getElementsByClassName("member");

		// Go through each country to get the name and center count
		for (var i = rows.length - 1; i >= 0; i--) {
			let span = rows[i].getElementsByClassName("memberLeftSide")[0];
			// console.log(rows[i]);
			let countryName = span.textContent.trim().toLowerCase();

			// Handles a weird dash during retreat phases
			if (countryName[0] == "-") {
				countryName = countryName.split(" ")[1];
			}

			let scCount = "0";
			try {
				let text = rows[i].getElementsByClassName("memberSCCount")[0].innerHTML;
				scCount = text.split("</em>")[0].substr(4);
			} catch(err) {
				// Error? Assume no field and 0 centers
				scCount = "0";
			}
			scrapedCenterCounts[countryName] = Number(scCount);
		}
	}

	// test URLs
	// Backstabbr, drawn game - https://www.backstabbr.com/game/Mar-Apr-Speednoat-Final1/6229962389716992
	// Backstabbr, won game - https://www.backstabbr.com/game/SB-Game-143/5154168504582144
	function parseBackstabbrGame(webpage, scrapedCenterCounts) {
		let legend = webpage.getElementsByClassName("legend")[0];
		let spans = legend.getElementsByTagName("span");
		for (var i = spans.length - 1; i >= 0; i--) {
			let strings = spans[i].textContent.trim().split(" ");
			if (strings.length === 2) {
				scrapedCenterCounts[strings[0].toLowerCase()] = Number(strings[1]);
			}
		}
	}

	// Set the scores from a given dictionary of {countryName: centerCount}
	function setCenterCounts(countryCountDict) {
		countryNames.forEach(country => {
			var countElement = document.getElementById(country + "-center-count");
			countElement.innerHTML = countryCountDict[country];
		});
		// Intentionally does *not* update scores here!
		// But does need to "get" aka refresh the centerCounts "global" variables
		// Yeah it's bad design, maybe I'll refactor in the future
		getCurrentCenterCounts();
		document.getElementById("total-center-count").innerHTML = totalCenters;
	}

	// Gets the current center counts from the HTML, called during updateCenterCounts()
	function getCurrentCenterCounts() {
		totalCenters = 0;

		countryNames.forEach(country => {
			var countElement = document.getElementById(country + "-center-count");
			centerCountsElements[country] = countElement;

			var num = Number(countElement.innerHTML);
			centerCountsNumbers[country] = num;
			totalCenters += num;
		});
	}

	// Update the center counts
	function updateCenterCounts(index, change) {
		getCurrentCenterCounts();

		var changeNum = Number(change);
		var newCount = centerCountsNumbers[index] + changeNum;
		centerCountsNumbers[index] = newCount;
		centerCountsElements[index].innerHTML = newCount;
		var newTotal = totalCenters + changeNum;
		document.getElementById("total-center-count").innerHTML = newTotal;

		if (newTotal >= 35) {
			document.getElementById("total-center-count").style.color = 'red';
			document.getElementById("total-center-count").style.fontWeight = 'bold';
		} else {	
			document.getElementById("total-center-count").style.color = '#111111';
			document.getElementById("total-center-count").style.fontWeight = 'normal';
		}

		// New center counts? Need to update the scores!
		updateScores();
	}

	// Update the scores for all the scoring systems
	function updateScores() {
		updatePPSCScores();
		updateSumOfSquaresScores();
		// updateMindTheGapScores();	// Commented out for now to make the spacing work with OMG
		// re-add this one I implement toggling scoring systems in and out
		updateOpenTributeScores();
		updateOpenMindTheGapScores();

		// More scoring systems to come in the future...
	}

	// Armada
	function updateArmadaScores() {

	}

	// PPSC scores are just...the center counts (I'm not sure what a solo counts as?)
	function updatePPSCScores() {
		let totalPPSC = 0;
		let isSolo = false;
		let soloCountry = null;
		countryNames.forEach(country => {
			document.getElementById(country + "-score-ppsc").innerHTML = 
				centerCountsNumbers[country];
			totalPPSC += Number(document.getElementById(country + "-score-ppsc").innerHTML);

			// Assume that the first solo is the only solo
			if (centerCountsNumbers[country] > 17) {
				isSolo = true;
				soloCountry = country;
			}
		});
		document.getElementById("total-score-ppsc").innerHTML = totalPPSC;

		// Handle "Winner Take All"/solo scenario
		if (isSolo) {
			countryNames.forEach(country => {
				document.getElementById(country + "-score-ppsc").innerHTML = "0";
			});
			document.getElementById(soloCountry + "-score-ppsc").innerHTML = "34";
		}
	}

	/* Sum of Squares (SoS)

		Solo:
		- The whole pot to the winner

		Draw:
		- (SC_count^2) / (sum of all players (SC_count^2)) * the pot
		- ^copied directly from webDip's explanation https://webdiplomacy.net/points.php#SoS
		- it's helpful if the pot is divisible by 7, so I'm going with a 70 point pot
	*/
	function updateSumOfSquaresScores() {
		let denominator = 0;
		let isSolo = false;
		let soloCountry = null;
		countryNames.forEach(country => {
			denominator += (centerCountsNumbers[country] ** 2 );

			// Assume that the first solo is the only solo
			if (centerCountsNumbers[country] > 17) {
				isSolo = true;
				soloCountry = country;
			}
		});

		// Handle "Winner Take All"/solo scenario, else do normal SoS scoring
		if (isSolo) {
			countryNames.forEach(country => {
				document.getElementById(country + "-score-sos").innerHTML = "0";
			});
			document.getElementById(soloCountry + "-score-sos").innerHTML = pot;
		} else {
			countryNames.forEach(country => {
				let score = ((centerCountsNumbers[country] ** 2) / denominator) * pot;
				document.getElementById(country + "-score-sos").innerHTML = round(score, 0.001).toFixed(2);
			});
		}
	}

	/* Mind the Gap!

		Solo: 
		- 100 points to winner
		- No points to others

		Draw:
		- 1.5 points for each SC controlled at the end of the game
		- 49 points to be split equally among survivors
		- Tribute paid to the board topper is equal to the difference between the SC count of 1st place and 2nd place. Tribute amount cannot exceed survival bonus. In the case of a shared top, there is no tribute.
	*/
	function updateMindTheGapScores() {
		var boardLeader = 'austria';
		var highestCount = 0;
		var secondHighestCount = 0;
		var sharedTop = false;
		var numSurvivors = 7;

		// Find the highest and second highest center counts, set points per SC
		Object.entries(centerCountsNumbers).forEach(([country, count]) => {
			if (count <= 0) {
				numSurvivors -= 1;
				count = 0;
			} else if (count > highestCount) {
				boardLeader = country;
				sharedTop = false;
				secondHighestCount = highestCount;
				highestCount = count;
			} else if (count === highestCount) {
				sharedTop = true;
				secondHighestCount = highestCount;
			} else if (count > secondHighestCount) {
				secondHighestCount = count;
			}
		});

		// If there's a solo, set solo point scoring and nothing else
		if (highestCount >= 18) {
			countryNames.forEach(country => {
				document.getElementById(country + "-score-mind-the-gap").innerHTML = 0;
			});
			document.getElementById(boardLeader + "-score-mind-the-gap").innerHTML = 100;
			document.getElementById("total-score-mind-the-gap").innerHTML = 100;
		}
		// Else, do the regular scoring
		else {
			var survivalBonus = 49.0 / numSurvivors;
			var tribute = 0.0;
			var tributeBonus = 0.0;
			var totalMindTheGap = 0.0;

			// Calculate tribute amount
			if (!sharedTop) {
				tribute = highestCount - secondHighestCount;
				// "Tribute amount cannot exceed survival bonus"
				if (tribute > survivalBonus) {
					tribute = round(survivalBonus, 0.001);
				}
			}

			// Handle tribute subtractions, no negative points, and total score
			Object.entries(centerCountsNumbers).forEach(([country, count]) => {
				var points = count * 1.5 + survivalBonus;

				// no tribute if you're the leader
				if (country !== boardLeader) {
					if (count <= 0) {	// dead? no points
						points = 0;
					} else if (points < tribute) {	// no negative points
						tributeBonus += points;
						points = 0;
					} else {
						tributeBonus += tribute;
						points -= tribute;
					}
				}

				document.getElementById(country + "-score-mind-the-gap").innerHTML = round(points, 0.001).toFixed(2);
				totalMindTheGap += points;
			});

			// Board leader tribute bonus
			if (!sharedTop) {
				document.getElementById(boardLeader + "-score-mind-the-gap").innerHTML = round(centerCountsNumbers[boardLeader] * 1.5 + survivalBonus + tributeBonus, 0.001).toFixed(2);
			}
			totalMindTheGap += tributeBonus;

			document.getElementById("total-score-mind-the-gap").innerHTML = round(totalMindTheGap, 0.001).toFixed(2);
			// console.log("survivalBonus: ", survivalBonus);
			// console.log("tribute: ", tribute);
			// console.log("tributeBonus: ", tributeBonus);
		}
	}

	/* OpenTribute*

		Explained here: https://diplobn.com/opentribute-scoring-system/

		Score = 34 + 3 × centers ± tribute – spoiled

		Solo:
		- 340 points to the winner
		- No points to others

		Draw:
		- 34 points for making a draw, 0 points for being eliminated
		- 3 points per SC controlled at the end of the game
		- "Each player pays the board-topper 1 point in tribute for each center they are behind the leader"
		- "If n players share the top, they split 1/n of the collected tribute, so that each receives 1/n^2.  Thus, a board-top and a second place will beat two shared-tops of similar size."
	*/
	function updateOpenTributeScores() {
		var boardLeaders = [];
		var highestCount = 0;

		// Find the highest and second highest center counts, set points per SC
		Object.entries(centerCountsNumbers).forEach(([country, count]) => {
			if (count <= 0) {
				count = 0;
			} else if (count > highestCount) {
				boardLeaders = [];
				boardLeaders.push(country);
				highestCount = count;
			} else if (count === highestCount) {
				boardLeaders.push(country);
			}
		});

		// If there's a solo, set solo point scoring and nothing else
		// This is modified to be smaller for the Nexus Speedboat League?
		// update as needed
		if (highestCount >= 18) {
			countryNames.forEach(country => {
				document.getElementById(country + "-score-open-tribute").innerHTML = 0;
			});
			document.getElementById(boardLeaders[0] + "-score-open-tribute").innerHTML = 340;
			document.getElementById("total-score-open-tribute").innerHTML = 340;
		}
		// Else, do the regular scoring
		else {
			var survivalBonus = 34.0;
			var tribute = 0.0;
			var centerDifference = 0.0;
			var totalOpenTribute = 0.0;

			// Handle tribute subtractions, no negative points, and total score
			Object.entries(centerCountsNumbers).forEach(([country, count]) => {
				var points = count * 3.0 + survivalBonus;

				// no tribute if you're the leader or tied for board top
				if (count !== highestCount) {
					if (count <= 0) {	// dead? no points, but pays tribute
						points = 0;
						tribute += highestCount;
					} else {
						centerDifference = highestCount - count;
						tribute += centerDifference;
						points -= centerDifference;
					}
				}

				document.getElementById(country + "-score-open-tribute").innerHTML = round(points, 0.001).toFixed(1);
				totalOpenTribute += points;
			});

			// Board leader tribute bonus
			var tributePortion = 0.0;
			if (boardLeaders.length === 1) {
				document.getElementById(boardLeaders[0] + "-score-open-tribute").innerHTML = round(highestCount * 3.0 + survivalBonus + tribute, 0.001).toFixed(1);
				tributePortion = tribute; 	// 1 leader, no spoiling
			} else {
				tributePortion = (tribute / boardLeaders.length) / boardLeaders.length;

				Object.entries(boardLeaders).forEach(([count, country]) => {
					document.getElementById(country + "-score-open-tribute").innerHTML = round(highestCount * 3.0 + survivalBonus + tributePortion, 0.001).toFixed(1);
				});
			}
			totalOpenTribute += tributePortion * boardLeaders.length;

			// console.log("totalOpenTribute: ", totalOpenTribute);

			document.getElementById("total-score-open-tribute").innerHTML = round(totalOpenTribute, 0.001).toFixed(0);
		}
	}

	/* Open Mind the Gap! OMG!

		Quote EVR1022 in the Nexus discord:

		5. Scoring system - Open Mind the Gap (OMG):
			a) Each supply center (SC) is worth 1.5 points (total = 51 points)
			b) Surviving in a draw is worth 9 points (average = 40.5 points per game)
			c) Bonuses for the Top 3: 4.5 points for 1st, 3 points for 2nd, 1.5 points for 3rd
			d) Tribute paid to the board topper is equal to 1st place SCs - 2nd place SCs, capped at 50% of a players score from a, b, and c
			e) A solo victory is currently worth 100 points currently in Nexus speedboat, but might differ for different competitions
	*/
	function updateOpenMindTheGapScores() {
		var firstPlacers = [];
		var secondPlacers = [];
		var thirdPlacers = [];
		var firstPlaceCount = 0.0;
		var secondPlaceCount = 0.0;
		var thirdPlaceCount = 0.0;
		var firstPlaceBonus = 4.5;
		var secondPlaceBonus = 3.0;
		var thirdPlaceBonus = 1.5;

		// Find the 1st, 2nd, and 3rd place countries, for their bonuses and the tribute
		// Current logic evenly splits the bonus points for each tied position amongst the tied countries
		Object.entries(centerCountsNumbers).forEach(([country, count]) => {
			if (count <= 0) {
				count = 0.0;
			} else if (count > firstPlaceCount) {
				// Do the shuffle
				thirdPlacers = secondPlacers;
				thirdPlaceCount = secondPlaceCount;
				secondPlacers = firstPlacers;
				secondPlaceCount = firstPlaceCount;

				firstPlacers = [];
				firstPlacers.push(country);
				firstPlaceCount = count;
			} else if (count === firstPlaceCount) {
				firstPlacers.push(country);
			} else if (count > secondPlaceCount) {
				// Do the shuffle
				thirdPlacers = secondPlacers;
				thirdPlaceCount = secondPlaceCount;

				secondPlacers = [];
				secondPlacers.push(country);
				secondPlaceCount = count;
			} else if (count === secondPlaceCount) {
				secondPlacers.push(country);
			} else if (count > thirdPlaceCount) {
				thirdPlacers = [];
				thirdPlacers.push(country);
				thirdPlaceCount = count;
			} else if (count === thirdPlaceCount) {
				thirdPlacers.push(country);
			}
		});

		// Determine the placing bonuses aka if any sharing is happening
		if (firstPlacers.length > 1) {
			if (firstPlacers.length === 2) {
				firstPlaceBonus = (4.5 + 3.0) / 2;
				secondPlaceBonus = 1.5 / secondPlacers.length;
				thirdPlaceBonus = 0.0;
			} else {
				firstPlaceBonus = (4.5 + 3.0 + 1.5) / firstPlacers.length;
				secondPlaceBonus = 0.0;
				thirdPlaceBonus = 0.0;
			}
		} else if (secondPlacers.length > 1) {
			secondPlaceBonus = (3.0 + 1.5) / secondPlacers.length;
			thirdPlaceBonus = 0.0;
		} else if (thirdPlacers.length > 1) {
			thirdPlaceBonus = thirdPlaceBonus / thirdPlacers.length;
		}

		// If there's a solo, set solo point scoring and nothing else
		// Just putting the text SOLO so that people check their league's solo value
		if (firstPlaceCount >= 18) {
			countryNames.forEach(country => {
				document.getElementById(country + "-score-omg").innerHTML = 0;
			});
			document.getElementById(firstPlacers[0] + "-score-omg").innerHTML = "SOLO";
			document.getElementById("total-score-omg").innerHTML = "SOLO";
		}
		// Else, do the regular scoring
		else {
			var survivalBonus = 9.0;
			var tribute = firstPlaceCount - secondPlaceCount;
			var totalTribute = 0.0;
			var totalOMG = 0.0;
			var halfPoints = 0.0;

			// If shared top, no tribute
			if (firstPlacers.length > 1) {
				tribute = 0.0;
			}

			// Handle tribute subtractions, no negative points, and total score
			Object.entries(centerCountsNumbers).forEach(([country, count]) => {
				var points = count * 1.5 + survivalBonus;

				if (count <= 0) {
					points = 0;
				}

				// Podium bonuses, divided equally between places
				if (firstPlacers.includes(country)) {
					points += firstPlaceBonus;
				} else {
					if (secondPlacers.includes(country)) {
						points += secondPlaceBonus;
					} else if (thirdPlacers.includes(country)) {
						points += thirdPlaceBonus;
					}
					// Pay tribute
					halfPoints = points / 2;
					if (tribute > halfPoints) {
						totalTribute += halfPoints
						points = halfPoints;
					} else {
						totalTribute += tribute;
						points -= tribute;
					}
				}

				document.getElementById(country + "-score-omg").innerHTML = round(points, 0.001).toFixed(2);
				totalOMG += points;
			});

			// Board leader tribute bonus
			if (firstPlacers.length === 1) {
				document.getElementById(firstPlacers[0] + "-score-omg").innerHTML = round(firstPlaceCount * 1.5 + survivalBonus + firstPlaceBonus + totalTribute, 0.001).toFixed(2);
				totalOMG += totalTribute;
			}

			// console.log("totalOMG: ", totalOMG);

			document.getElementById("total-score-omg").innerHTML = round(totalOMG, 0.001).toFixed(2);
		}
	}

	// From this S/O answer: https://stackoverflow.com/a/34591063/2521402
	function round(value, step) {
	    step || (step = 1.0);
	    var inv = 1.0 / step;
	    return Math.round(value * inv) / inv;
	}

	// Toggle displaying a scoring system
	function toggleScoringSystem(scoringSystemClassName) {
		let elements = document.getElementsByClassName(scoringSystemClassName);
		let usesYearElim = false;
		let willBeVisible = true; // don't trust this initial value

		// Manually check each system that uses it
		if (scoringSystemClassName === "armada") {
			usesYearElim = true;
		}

		Array.from(elements).forEach(element => {
			if (window.getComputedStyle(element).display === "none") {
				element.style.display = "table-cell";
				willBeVisible = true;
			} else {
				element.style.display = "none";
				willBeVisible = false;
			}
		});

		// If this scoring system uses year eliminated, toggle it
		if (usesYearElim) {
			if (willBeVisible) {
				// Only need to toggle if it's not already visible
				if (yearElimSystemsCount === 0) {
					toggleYearElim();
				}
				yearElimSystemsCount += 1;
			} else {
				// Only turn it off if no more visible systems are using it
				if (yearElimSystemsCount === 1) {
					toggleYearElim();
				}
				yearElimSystemsCount -= 1;
			}
		}
	}

	// Helper function for year-eliminated elements
	function toggleYearElim() {
		let elements = document.getElementsByClassName("year-eliminated");

		Array.from(elements).forEach(element => {
			if (window.getComputedStyle(element).display === "none") {
				element.style.display = "table-cell";
			} else {
				element.style.display = "none";
			}
		});
	}

</script>

{{< /rawhtml >}}
