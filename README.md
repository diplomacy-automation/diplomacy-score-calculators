# Diplomacy Score Calculators

## https://diplomacy-things.gitlab.io/diplomacy-score-calculators/

### A website for calculating the points of various Diplomacy scoring systems

NEXT TASKS:

- more scoring systems (maybe)
  - http://armada-dip.com/scoring.html

---

TODO:
- text box for entering raw numbers?

- change country colors to match a specific website (radio button toggle), have this saved via cookie!

- scoring system explanations lower down

- adjustable pot size for applicable scoring systems

- ^related, consider moving the pot field to the top left of the table

- I glanced at PlayDip and it doesn't seem possible...no idea if the center counts are in text anywhere on the game page

- Some kind of traffic tracking, not logging info just # visitors and stuff like that. I don't want Google Analytics-esque stuff, basically, but a counter would be nice.

- Move JS into separate files so it's more clear where it lives

- Move the HTML as well, into just 1 big template or something, since it's just stuffed into "\_index.md" right now

---

DONE (newest features at the top):
- Focus on URL text box immediately on loading the page
- Able to toggle displaying scoring systems
- Website is no longer mobile hostile, on its way to mobile friendly
- buttons look much better on Firefox
- Open Mind the Gap scoring
- OpenTribute scoring
- PPSC scoring
- buttons to adjust the center counts
- basic country colors per row
- Mind the Gap scoring
- text turns red and bold when total center count is >= 35
- able to submit a game link and have the page scrape the current center counts and give back the scores
- Added a gif as the favicon
- parse Backstabbr, webDip, vDip

---

Local debugging:
- `hugo server -D` will launch the site on localhost:1313


Process for adding a new scoring system:
- Decide what string to use for its name in the code
- Add a new toggle button for the new scoring system
- Add new <td> entries in each "scoring-row" section
- Implement the new scoring system